The script `mkdirs.sh` reads a list of usernames, creates directories for them and copies the skeleton files. It's horribly broken and made one server unbootable. Explain why and how should it be fixed.


## solution


 there was a issue with the destination folder name original variable is with uppercase ( linux is case sensitive )
 secondly the file with the users contains some "invalid" characters like slash and space( which can be interpreted as a new line or seperate string )
 after fixing the destination folder variable , usernames should be "sanitized" i used "detox" ( which can be installed with apt ) program which is easy to use 
 if we do not have program installation privileges in can be done with a script which removes invalid characters
 script can make system unbootable because the way it treats the / ( filesystem root )  -  f** ups the ownership of all files in the system


i made a copy of the original script and modified it modifications made in order to run without errors can be seen in the patch file 

modified script:
``` bash
#!/bin/bash

#echo DO NOT RUN, IT BROKE srv05
#exit 5


DST=/tmp/adsf
mkdir -p $DST


(while /bin/true ; do
        read usr
        if [ -z "$usr" ]; then
                break
        fi
        usr=$(echo $usr | detox --inline )
        mkdir -v -p $DST/$usr
        cp -a skel $DST/$usr
        chown -R bin.bin $DST/$usr

done) <userlist


```

patch file
``` patch
--- mkdirs.sh   2017-04-08 15:31:57.000000000 -0500
+++ test.sh     2022-06-01 02:49:17.459844153 -0500
@@ -1,7 +1,7 @@
 #!/bin/bash

-echo DO NOT RUN, IT BROKE srv05
-exit 5
+#echo DO NOT RUN, IT BROKE srv05
+#exit 5


 DST=/tmp/adsf
@@ -13,8 +13,9 @@
        if [ -z "$usr" ]; then
                break
        fi
+        usr=$(echo $usr | detox --inline )
        mkdir -v -p $DST/$usr
-       cp -a skel $dst/$usr
-       chown -R bin.bin $dst/$usr
+       cp -a skel $DST/$usr
+       chown -R bin.bin $DST/$usr

 done) <userlist
```
