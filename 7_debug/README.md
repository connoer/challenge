There are two programs in this directory - `a` and `b`. They both crash. Explain why.

Bonus points for discovering the password of `b`.

# ./a
program a crashes beacuse of missing file "pesho"

``` bash
root@debian11:~/challenge/7_debug_NO_SOLUTION# strace ./a
execve("./a", ["./a"], 0x7ffe80aa5950 /* 22 vars */) = 0
brk(NULL)                               = 0xdee000
brk(0xdef1c0)                           = 0xdef1c0
arch_prctl(ARCH_SET_FS, 0xdee880)       = 0
uname({sysname="Linux", nodename="debian11", ...}) = 0
readlink("/proc/self/exe", "/root/challenge/7_debug_NO_SOLUT"..., 4096) = 37
brk(0xe101c0)                           = 0xe101c0
brk(0xe11000)                           = 0xe11000
openat(AT_FDCWD, "./pesho", O_RDONLY)   = -1 ENOENT (No such file or directory)
read(-1, 0x7ffc9bff5980, 1024)          = -1 EBADF (Bad file descriptor)
close(-1)                               = -1 EBADF (Bad file descriptor)
--- SIGSEGV {si_signo=SIGSEGV, si_code=SEGV_MAPERR, si_addr=NULL} ---
+++ killed by SIGSEGV +++
Segmentation fault
root@debian11:~/challenge/7_debug_NO_SOLUTION#
```


# ./b

probram b crashes becauase of missing env variable called TMPDIR

```
export TMPDIR=example_dir
```
programs continues to work and asks for password 



# password for the b is: 

```
root@debian11:~/challenge/7_debug# export TMPDIR=1
root@debian11:~/challenge/7_debug# ./b
Init done 0 .
Enter password: mnogoslozhnaparolaamanainstina

OK.
root@debian11:~/challenge/7_debug#


```




after nop-ping the following part the segmentation fault error went away


```
 >0x401cf6 <main+185>     movzx  eax,BYTE PTR [rax]
```

``` bash
root@debian11:~/challenge/7_debug# ./c
Init done 0 .
Enter password: hello

WRONG
root@debian11:~/challenge/7_debug#
```


```
