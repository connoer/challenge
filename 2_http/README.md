Make a HTTP request to https://spnagios.storpool.com/localweb/meow.php with parameter "hi" to equal two_unique from the previous task, without a browser or curl and you'll get an email address.


## solution 

using gnutls-cli ( openssl can be used too ) 

we estabilish a tls connection to the host and enter an GET reqeust manually

```
GET /localweb/meow.php?hi=8 HTTP/1.1
Host: spnagios.storpool.com
```
