cloud_user@0490c9ffc5344635b072d3a8db3421bc1c:~$ gnutls-cli spnagios.storpool.com
Processed 129 CA certificate(s).
Resolving 'spnagios.storpool.com'...
Connecting to '2a05:5e40:f00f:10:0:cafe:cafe:80:443'...
- Certificate type: X.509
- Got a certificate list of 3 certificates.
- Certificate[0] info:
 - subject `CN=spnagios.storpool.com', issuer `C=US,O=Let's Encrypt,CN=R3', RSA key 4096 bits, signed using RSA-SHA256, activated `2022-04-09 04:24:08 UTC', expires `2022-07-08 04:24:07 UTC', SHA-1 fingerprint `f271ee5567d66e4930a92547e3af423aa34879ec'
        Public Key ID:
                09fb6997bce6bdbab0aad1ca7781ab178deb0a3e
        Public key's random art:
                +--[ RSA 4096]----+
                |                 |
                |                 |
                |      .          |
                |       o .       |
                |      .+S        |
                |     .+.oo .     |
                |  . . .+=.+      |
                | .Eo o=..+.o     |
                |  ..*Boo.o=oo.   |
                +-----------------+

- Certificate[1] info:
 - subject `C=US,O=Let's Encrypt,CN=R3', issuer `C=US,O=Internet Security Research Group,CN=ISRG Root X1', RSA key 2048 bits, signed using RSA-SHA256, activated `2020-09-04 00:00:00 UTC', expires `2025-09-15 16:00:00 UTC', SHA-1 fingerprint `a053375bfe84e8b748782c7cee15827a6af5a405'
- Certificate[2] info:
 - subject `C=US,O=Internet Security Research Group,CN=ISRG Root X1', issuer `O=Digital Signature Trust Co.,CN=DST Root CA X3', RSA key 4096 bits, signed using RSA-SHA256, activated `2021-01-20 19:14:03 UTC', expires `2024-09-30 18:14:03 UTC', SHA-1 fingerprint `933c6ddee95c9c41a40f9f50493d82be03ad87bf'
- Status: The certificate is trusted.
- Description: (TLS1.2)-(ECDHE-RSA-SECP256R1)-(AES-256-GCM)
- Session ID: 81:0A:77:0A:DF:29:C3:6D:01:18:BD:1A:2B:2E:52:0C:8A:72:D1:C6:D9:C8:6C:99:4E:3E:28:92:14:9E:C0:B4
- Ephemeral EC Diffie-Hellman parameters
 - Using curve: SECP256R1
 - Curve size: 256 bits
- Version: TLS1.2
- Key Exchange: ECDHE-RSA
- Server Signature: RSA-SHA256
- Cipher: AES-256-GCM
- MAC: AEAD
- Compression: NULL
- Options: extended master secret, safe renegotiation,
- Handshake was completed

- Simple Client Mode:

GET /localweb/meow.php?hi=8 HTTP/1.1
Host: spnagios.storpool.com

HTTP/1.1 200 OK
Server: nginx/1.14.2
Date: Wed, 01 Jun 2022 08:25:38 GMT
Content-Type: text/html; charset=UTF-8
Transfer-Encoding: chunked
Connection: keep-alive

2d
Secret email address 571b322ed@storpool.com

0
