In the directory ... there is a sub-directory named `cfg`, a passwd file, and a validator called `validate.sh`.

Write a script that:

- validates all files in `cfg` with `validate.sh`;
- reports which files are broken and where;
- Creates one file from all sucessfully validated config files (concatenating them) in config.cfg;
- Checks if for all valid `cfg` file names if it present as an user name in the passwd file and if not, adds it with a random password, which it also prints out.

The passwd file format is `username:hashed_password`, the password is hashed using `mkpasswd`.

The script should be able to handle new config pieces added to the `cfg` directory.

## solution 

``` bash

#!/bin/bash
errorstring="errors"
> config.cfg;
for files in $(ls cfg); do
	result=$(./validate.sh cfg/$files );
		if [[ "$result" == *"$errorstring"* ]];
	 		then
	 			echo -n "file: [" $files "] is containing errors at :";
	 			echo  $result | cut -f2 -d ":";
	 	else
	 		cat cfg/$files >> config.cfg;
	 	fi

	 	if ! grep "$files" passwd > /dev/null;
	 		then
	 			echo -n "missing [ $files ]  in passwd, adding with new password:  ";
	 			password=$(cat /dev/urandom | head -c 12 | base64 );
	 			echo  " $password";
	 			hashedpassword=$(mkpasswd -m descrypt $password);
	 			echo "$files:$hashedpassword" >> passwd;
	 	fi 
done
```
