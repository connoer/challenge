#!/bin/bash
errorstring="errors"
> config.cfg;
for files in $(ls cfg/); do
	result=$(./validate.sh cfg/$files );
		if [[ "$result" == *"$errorstring"* ]];
	 		then
	 			echo -n "file: [" $files "] is containing errors at :";
	 			echo  $result | cut -f2 -d ":";
	 	else
	 		cat cfg/$files >> config.cfg;
	 	fi

	 	if ! grep "$files" passwd > /dev/null;
	 		then
	 			echo -n "missing [ $files ]  in passwd, adding with new password:  ";
	 			password=$(cat /dev/urandom | head -c 12 | base64 );
	 			echo  " $password";
	 			hashedpassword=$(mkpasswd -m descrypt $password);
	 			echo "$files:$hashedpassword" >> passwd;
	 	fi 
done

