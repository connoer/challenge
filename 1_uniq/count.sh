#!/bin/bash
for dirs in $(ls -d */ ) ; do  
	echo "folder: $dirs";
	allfiles=$(find $dirs -type f | wc -l);
	echo "all files: " $allfiles;
	
	uniqfiles=$(find $dirs -type f -print0 | while IFS= read -r -d '' file; do 
		md5sum "${file}"; 
	done  | awk '{print $1}' | sort --uniq | wc -l);
	echo "uniq files: " $uniqfiles ; printf -- '-%.0s' {1..30};printf '\n' ;
done
