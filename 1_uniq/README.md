There are three directories, named "one", "two" and "three".

For each of the directories:
Count all files and all files with unique content in it and all sub-directories.

## count.sh

``` bash
#!/bin/bash
for dirs in $(ls -d */ ) ; do
        echo "folder: $dirs";
        allfiles=$(find $dirs -type f | wc -l);
        echo "all files: " $allfiles;

        uniqfiles=$(find $dirs -type f -print0 | while IFS= read -r -d '' file; do
                md5sum "${file}";
        done  | awk '{print $1}' | sort --uniq | wc -l);
        echo "uniq files: " $uniqfiles ; printf -- '-%.0s' {1..30};printf '\n' ;
done
```

## output


``` bash
root@debian11:~/challenge/1_uniq# ./count.sh
folder: one/
all files:  9
uniq files:  3
------------------------------
folder: three/
all files:  60
uniq files:  5
------------------------------
folder: two/
all files:  297
uniq files:  8
------------------------------
```
