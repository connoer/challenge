install mhash library from source forge -  download , configure , make , make install
gcc -o gen  gen.c -lmhash


program generates two strings 
first one is random string  seperated with - every 4th symbol,  default length is 7 ( #define DEFAULT_KEY_LEN 7) , 
if an argument is passed to the program ( integer ) it overrites this variable and creates a string based on the argument passed

``` C
root@playground:[5_compile]:# ./gen 6
77T-MZ
A486B74E911D1D2F7B9628B2F8CBE94C
```


second string is fixed in length and always returns 32 characters

contents of the generated strings are based on unsigned char *psbl = "123456789ABCDEFGHIJKLMNPQRSTWXYZZZ";

if we want to contain other symbols we can change this string
